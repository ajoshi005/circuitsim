//Tests the svm classifier with image files from dataset.
//Current accuracy on validation dataset: 93.33% (112/120) (change after every run) 

#include <iostream>
#include <cv.h>
#include <highgui.h>
#include "opencv2/ml/ml.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/legacy/legacy.hpp"
#include <string>
#include <fstream>
#include "HarrisDetector.cpp"
//#include "highDensity.cpp"

using namespace cv;
using namespace std;

int main()
{

//Test
	CvSVM svm;
	svm.load("classifier.xml","Components");
	//Mat testImg = imread("Test");
	
	Mat vocabulary;
	
	FileStorage fs3("vocabulary.xml", FileStorage::READ);
	cout<<"Reading Vocabulary from file";
	
	fs3["vocabulary"]>>vocabulary;
	fs3.release();
	chdir("Test");
	ifstream f1;
	f1.open("list.txt");
	ofstream f2("output_vlad.txt");
	while(!f1.eof())
	{
		string imgName;
		f1>>imgName;
		cout<<"\n"<<imgName;
		Mat img=imread(imgName);
		if(!img.data)
			continue;
		Ptr<FeatureDetector> detector(new SiftFeatureDetector());
		Ptr<DescriptorExtractor> extractor(new SiftDescriptorExtractor());
		Ptr<DescriptorMatcher> matcher(new BruteForceMatcher<L2 <float> >() );
		BOWImgDescriptorExtractor bowide(extractor,matcher);
		bowide.setVocabulary(vocabulary);
		vector<KeyPoint> keypoints;
		Mat desc(0,30,CV_64FC1);
		
		cvtColor(img,img,CV_BGR2GRAY);
		SIFT sift;
		vector<Point2f> points;
		harrisFeatures(img, points);
		//cout<<"\n POints size:"<<points.size();
		int i;

		for(i=0;i<points.size();i++)
		{
				KeyPoint temp(points[i],10,-1,0,0,-1);
				keypoints.push_back(temp);
				//cout<<"\n Point "<<i<<" "<< keypoints[i].pt.x <<" "<<keypoints[i].pt.y; 
		}
        Mat responseHist;
		sift(img,img,keypoints,desc,true);
		bowide.compute(img,keypoints,responseHist);
	
				
			
		
		float predLabel=5;
		if(responseHist.rows!=0)
		{
			predLabel = svm.predict(responseVector, false);
		}
		cout<<"\n value:"<<predLabel;
		f2 << imgName << " " << predLabel << "\n"; 
	}	
	
	f2.close();
	return 0;
}	
