//BOWKmeans training- Creates BOW vocabulary(codebook)
//Clusters the features using k-means into a codebook. 
//Requires features be generated using sift.cpp and placed in a dir. 
//TODO: Clean up teh code and make it an independent module after testing.


#include <highgui.h>
#include "opencv2/opencv.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include "HarrisDetector.cpp"

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{	
	string fname;
	Mat trainDesc;
	chdir("SIFT");
	ifstream f1;
	f1.open("siftList.txt");
	
	//char opt = 'y';
	while(!f1.eof())
	{
		f1 >> fname;
		cout<<"Adding descriptors from "<<fname;
		FileStorage fs2(fname,FileStorage::READ);
		Mat add1;
		fs2["desc_all"] >> add1;
		cout<<"\n"<<add1.rows;
		trainDesc.push_back(add1);
	}
	cout<<"\n Train desc"<<trainDesc.rows;
	BOWKMeansTrainer bowtrainer(1000);
	bowtrainer.add(trainDesc);
	cout<<"\n clustering Bow features"<<endl;
	
	Mat vocabulary = bowtrainer.cluster();
	chdir("..");
	FileStorage fs1("vocabulary_sift.xml",FileStorage::WRITE); //Stores codebook
	
	fs1<<"vocabulary"<<vocabulary;
	
	fs1.release();
	f1.close();
	return 0;
}
