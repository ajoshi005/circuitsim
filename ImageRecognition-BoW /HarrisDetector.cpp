//Primary function for detecting harris keypoints. Further onused for DBScan clustering and object recognition
void harrisFeatures(Mat &orig_img, vector<Point> &points)
{
	Mat img_blur, img_gray;
	
	GaussianBlur(orig_img, img_blur, Size(3,3), 0, 0, BORDER_DEFAULT);
	
	cvtColor(img_blur, img_gray, CV_RGB2GRAY);
	
	Mat Ix, Iy;
	
	Sobel(img_gray, Ix, CV_32F, 1, 0, 3, 1, 0, BORDER_DEFAULT);
	Sobel(img_gray, Iy, CV_32F, 0, 1, 3, 1, 0, BORDER_DEFAULT);
	
	Mat Ix_Ix, Iy_Iy, Ix_Iy;
	
	Ix_Ix = Ix.mul(Ix);
	Iy_Iy = Iy.mul(Iy);
	Ix_Iy = Ix.mul(Iy);
	
	Mat Ix_Ix_blur, Iy_Iy_blur, Ix_Iy_blur;
	
	GaussianBlur(Ix_Ix, Ix_Ix_blur, Size(5,5), 0, 0, BORDER_DEFAULT);
	GaussianBlur(Iy_Iy, Iy_Iy_blur, Size(5,5), 0, 0, BORDER_DEFAULT);
	GaussianBlur(Ix_Iy, Ix_Iy_blur, Size(5,5), 0, 0, BORDER_DEFAULT);
	
	int i, j, k, l;
	
	orig_img.copyTo(harris_features);
	
	float harris_threshold, alpha = 0.06;
	
	harris_threshold = 5000000;
	
	Mat detA, traceA, harris_function;
	
	Mat feature_points(orig_img.rows, orig_img.cols, CV_8UC1);
	
	detA = Ix_Ix_blur.mul(Iy_Iy_blur) - Ix_Iy_blur.mul(Ix_Iy_blur);
	traceA = Ix_Ix_blur + Iy_Iy_blur;
	harris_function = detA - alpha*traceA.mul(traceA);
	
	for(i = 0; i < Ix.rows; i++)
	{
		for(j = 0; j < Ix.cols; j++)
		{
			if(harris_function.at<float>(i, j) > harris_threshold)
			{
				feature_points.at<uchar>(i, j) = 255;			
			}
			else
			{
				feature_points.at<uchar>(i, j) = 0;
			}
		}
	}
	
	for(i = 0; i < Ix.rows; i++)
	{
		for(j = 0; j < Ix.cols; j++)
		{
			if(feature_points.at<uchar>(i, j) == 255)
			{
				for(k = i - filter_size; k <= i + filter_size; k++)
				{
					for(l = j - filter_size; l <= j + filter_size; l++)
					{
						if(k > -1 && l > -1 && k < Ix.rows && l < Ix.cols && (k != i && l != j))
						{
							feature_points.at<uchar>(k, l) = 0;
						}
					}
				}
				
				//circle(harris_features, Point(j, i), 2,  Scalar(0, 0, 255), -1, 8);
				
				points.push_back(Point(i, j));
				
				dataset_size++;
			}
		}
	}
}
