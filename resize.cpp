#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

int main(int argc, char *argv[])
{
	Mat orig_img;
	
	orig_img = imread(argv[1]);
	
	resize(orig_img, orig_img, Size(orig_img.cols/atoi(argv[2]), orig_img.rows/atoi(argv[2])));
	
	imwrite(argv[1], orig_img);
}
