/*Sourcecode for Preprocessing the circuit image and localizing the components.
 * Major Functionality included: 
 * DBScan - Density based clustering algorithm for localiation
 * determinePotentials - determine the junctions with similar potentials after removing components
 * establishConnectivity - Function for establishing the connectivity between components
 */  
//TODO: Code doesnot work properly incase of diode images. Harris Keypoints need to substituted with a better alternative. Dense SIFT???

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

int epsilon = 1600;
int min_points = 12;
int features = 2;
int dataset_size;
int filter_size;
int potentials;
vector<int> done;
vector<Vec2i> points;
vector<Vec2i> ends;
vector<Vec2i> junction_points;
vector< vector<Vec2i> > same_potentials;

int **data;
int *clusters;
int *visited;
int *neigh_points;

Mat orig_img, harris_features, gray_image;
//Find the junctions with the same potentials. Simple line growing algorithm
void determinePotentials(int junctions, int connectivity[], int start)
{
	int j;
	
	if(done[start] == 0)
	{
		done[start] = 1;
		
		for(j = start+1; j < junctions; j++)
		{
			if(connectivity[start*junctions + j] == 1)
			{
				same_potentials[potentials - 1].push_back(junction_points[j]);
			
				determinePotentials(junctions, connectivity, j);
			}
		}
	}
}

int checkConnectivity(int p1, int p2)
{
	int dx, dy, i, x, y, count = 0, gt_1, j;
	
	dx = junction_points[p1][0] - junction_points[p2][0];
	dy = junction_points[p1][1] - junction_points[p2][1];
	
	float slope;
	
	if(dx)
	{
		slope = (dy * 1.0) / dx;
		
		if(slope > 1 || slope < -1)
		{
			gt_1 = 1;
		}
		else
		{
			gt_1 = 0;
		}
	}
	else
	{
		gt_1 = 1;
	}
			
	for(i = 1; i < 50; i++)
	{
		x = cvRound(junction_points[p2][0] * 1.0 + (dx * 1.0 * i) / 50.0);
		
		y = cvRound(junction_points[p2][1] * 1.0 + (dy * 1.0 * i) / 50.0);
		
		if(gt_1)
		{
			for(j = x-10; j <= x+10; j++)
			{
				if(j > -1 && j < harris_features.rows && gray_image.at<uchar>(j, y) < 130)
				{
					count++;
					break;
				}
			}
		}
		else
		{
			for(j = y-10; j <= y+10; j++)
			{
				if(j > -1 && j < harris_features.cols && gray_image.at<uchar>(x, j) < 130)
				{
					count++;
					break;
				}
			}
		}
	}
	
	if(count >= 46)
		return 1;
	else
		return 0;
}
//Function for determining junctions
void determineJunctions(int junctions)
{
	int i, count[junctions], temp;
	
	for(i = 0; i < junctions; i++)
	{
		junction_points.push_back(Point(0,0));
		count[i] = 0;
	}
	
	for(i = 0; i < dataset_size; i++)
	{
		temp = clusters[i];
		
		if(temp)
		{
			junction_points[temp-1][0] += data[i][0];
			junction_points[temp-1][1] += data[i][1];
			count[temp-1]++;
		}
	}
	
	for(i = 0; i < junctions; i++)
	{
		junction_points[i][0] /= count[i];
		junction_points[i][1] /= count[i];
		
		//circle(harris_features, Point(junction_points[i][1], junction_points[i][0]), 2,  Scalar(0, 0, 255), -1, 8);
	}
}

void freeData()
{
	points.clear();
	ends.clear();
	
	free(data);
	free(clusters);
	free(visited);
	free(neigh_points);
	
	dataset_size = 0;
}

void determineRectangles(int rectangles)
{
	int i, temp;
	
	for(i = 0; i < rectangles; i++)
	{
		ends.push_back(Point(1000000,1000000));
		ends.push_back(Point(0,0));
	}
	
	for(i = 0; i < dataset_size; i++)
	{
		temp = clusters[i];
		
		if(temp)
		{
			if(data[i][0] < ends[2*(temp-1)][0])
			{
				ends[2*(temp-1)][0] = data[i][0];
			}
			if(data[i][1] < ends[2*(temp-1)][1])
			{
				ends[2*(temp-1)][1] = data[i][1];
			}
			if(data[i][0] > ends[2*(temp-1)+1][0])
			{
				ends[2*(temp-1)+1][0] = data[i][0];
			}
			if(data[i][1] > ends[2*(temp-1)+1][1])
			{
				ends[2*(temp-1)+1][1] = data[i][1];
			}
		}
	}
}
//Data printnig unction to pass to object recog module for now. TODO: Integrte object recog after improving results
void printData()
{
	int i, j;
	
	FILE *fp;
	fp = fopen("prediction.txt", "w");
	
	for(i = 0; i < dataset_size; i++)
	{
		fprintf(fp, "%d\n", clusters[i]);
	}
}
//DBScan helper function
int regionQuery(int start, int index)
{
	int i, j, count = 0, distance, temp;
	
	for(i = 0; i < dataset_size; i++)
	{
		if(i != index)
		{
			distance = 0;
		
			for(j = 0; j < features; j++)
			{
				temp = data[i][j] - data[index][j];
			
				distance += temp * temp;
			}
		
			if(distance <= epsilon)
			{
				neigh_points[start+count] = i;
			
				count++;
			}
		}
	}
	
	return count;
}
//DBScan helper function to iteratively expand the clusters
void expandCluster(int cluster_no, int num_npoints, int index)
{
	clusters[index] = cluster_no;
	
	int i, count = 0;
	
	for(i = 0; i < num_npoints; i++)
	{
		if(!visited[neigh_points[i]])
		{
			visited[neigh_points[i]] = 1;
			
			count = regionQuery(num_npoints, neigh_points[i]);
			
			if(count >= min_points)
			{
				num_npoints += count;
			}
		}
		
		if(!clusters[neigh_points[i]])
		{
			clusters[neigh_points[i]] = cluster_no;
		}
	}
}
//Density based Clustering for component localization.
int DBSCAN()
{
	int next_cluster = 1, i, j, num_npoints;
	
	for(i = 0; i < dataset_size; i++)
	{
		if(!visited[i])
		{
			visited[i] = 1;
			
			num_npoints = regionQuery(0, i);
			
			if(num_npoints > min_points)
			{
				expandCluster(next_cluster, num_npoints, i);
				
				next_cluster++;
			}
		}
	}
	
	return (next_cluster-1);
}
//Load previous iteration data
void loadData()
{
	data = (int**)calloc(dataset_size, sizeof(int*));

	visited = (int*)calloc(dataset_size, sizeof(int));
	
	neigh_points = (int*)calloc(dataset_size*dataset_size, sizeof(int));
	
	clusters = (int*)calloc(dataset_size, sizeof(int));
	
	int i, j;
	
	for(i = 0; i < dataset_size; i++)
	{
		data[i] = (int*)calloc(features, sizeof(int));
	}
	
	for(i = 0; i < dataset_size; i++)
	{
		for(j = 0; j < features; j++)
		{
			data[i][j] = points[i][j];
		}
	}
}
//Function for detecting triangular components, primarily filled in diodes. Buggy-requires more testing
void detectTriangles()
{
	int i;

	Mat img_gray, canny;
	
	cvtColor(orig_img, img_gray, CV_RGB2GRAY);
	
	vector< vector<Point> > contours;
	
	threshold(img_gray, img_gray, 0, 127, THRESH_BINARY); 
	Mat skel(img_gray.size(), CV_8UC1, Scalar(0));
	Mat temp;
	Mat eroded;
	 
	Mat element = getStructuringElement(MORPH_CROSS, Size(3, 3));
	
	bool done;
	do
	{
		morphologyEx(img_gray, temp, MORPH_OPEN, element);
		bitwise_not(temp, temp);
		bitwise_and(img_gray, temp, temp);
		bitwise_or(skel, temp, skel);
		erode(img_gray, img_gray, element);
		
		//imshow("Temp", img_gray);
		//waitKey(0);

		double max;
		minMaxLoc(img_gray, 0, &max);
		done = (max == 0);
	} while (!done);
	
	imshow("Skeleton", skel);
	waitKey(0);
	
	//Canny(img_gray, canny, 255, 510, 3);
	
	//imwrite("Canny.jpg", canny);
	
	findContours(skel, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	
	vector<Point> approxTriangle;
	
	for(i = 0; i < contours.size(); i++)
	{
        approxPolyDP(contours[i], approxTriangle, arcLength(Mat(contours[i]), true)*0.05, true);
        
        if(approxTriangle.size() == 3)
        {
            drawContours(harris_features, contours, i, Scalar(0, 255, 255), CV_FILLED);
        }
    }
}
// Function for dectecting sources and circular components. 
void detectCircles()
{
	int i, j, k, x, y;

	Mat img_blur;

	cvtColor(orig_img, img_blur, CV_RGB2GRAY);
	
	GaussianBlur(img_blur, img_blur, Size(9,9), 0, 0, BORDER_DEFAULT);
	
	vector<Vec3f> circles;
    
    HoughCircles(img_blur, circles, CV_HOUGH_GRADIENT, 2, img_blur.rows/4, 300, 80, 5, 100);
	
	for(i = 0; i < circles.size(); i++)
	{
		for(j = 0; j < 50; j++)
		{
			for(k = 1; k < 3; k++)
			{
				x = cvRound(circles[i][1] + (circles[i][2]*sin(j*0.1256) / k));
				
				y = cvRound(circles[i][0] + (circles[i][2]*cos(j*0.1256) / k));
				
				//circle(harris_features, Point(y, x), 2,  Scalar(0, 0, 255), -1, 8);
				
				points.push_back(Point(x, y));
				
				dataset_size++;
			}
		}
	}
}
//Primary function for detecting harris keypoints. Further onused for DBScan clustering and object recognition
void harrisFeatures()
{
	Mat img_blur, img_gray;
	
	GaussianBlur(orig_img, img_blur, Size(3,3), 0, 0, BORDER_DEFAULT);
	
	cvtColor(img_blur, img_gray, CV_RGB2GRAY);
	
	Mat Ix, Iy;
	
	Sobel(img_gray, Ix, CV_32F, 1, 0, 3, 1, 0, BORDER_DEFAULT);
	Sobel(img_gray, Iy, CV_32F, 0, 1, 3, 1, 0, BORDER_DEFAULT);
	
	Mat Ix_Ix, Iy_Iy, Ix_Iy;
	
	Ix_Ix = Ix.mul(Ix);
	Iy_Iy = Iy.mul(Iy);
	Ix_Iy = Ix.mul(Iy);
	
	Mat Ix_Ix_blur, Iy_Iy_blur, Ix_Iy_blur;
	
	GaussianBlur(Ix_Ix, Ix_Ix_blur, Size(5,5), 0, 0, BORDER_DEFAULT);
	GaussianBlur(Iy_Iy, Iy_Iy_blur, Size(5,5), 0, 0, BORDER_DEFAULT);
	GaussianBlur(Ix_Iy, Ix_Iy_blur, Size(5,5), 0, 0, BORDER_DEFAULT);
	
	int i, j, k, l;
	
	orig_img.copyTo(harris_features);
	
	float harris_threshold, alpha = 0.06;
	
	harris_threshold = 5000000;
	
	Mat detA, traceA, harris_function;
	
	Mat feature_points(orig_img.rows, orig_img.cols, CV_8UC1);
	
	detA = Ix_Ix_blur.mul(Iy_Iy_blur) - Ix_Iy_blur.mul(Ix_Iy_blur);
	traceA = Ix_Ix_blur + Iy_Iy_blur;
	harris_function = detA - alpha*traceA.mul(traceA);
	
	for(i = 0; i < Ix.rows; i++)
	{
		for(j = 0; j < Ix.cols; j++)
		{
			if(harris_function.at<float>(i, j) > harris_threshold)
			{
				feature_points.at<uchar>(i, j) = 255;			
			}
			else
			{
				feature_points.at<uchar>(i, j) = 0;
			}
		}
	}
	
	for(i = 0; i < Ix.rows; i++)
	{
		for(j = 0; j < Ix.cols; j++)
		{
			if(feature_points.at<uchar>(i, j) == 255)
			{
				for(k = i - filter_size; k <= i + filter_size; k++)
				{
					for(l = j - filter_size; l <= j + filter_size; l++)
					{
						if(k > -1 && l > -1 && k < Ix.rows && l < Ix.cols && (k != i && l != j))
						{
							feature_points.at<uchar>(k, l) = 0;
						}
					}
				}
				
				//circle(harris_features, Point(j, i), 2,  Scalar(0, 0, 255), -1, 8);
				
				points.push_back(Point(i, j));
				
				dataset_size++;
			}
		}
	}
}
//Primary function for removing localied components. Helps in easy identification of connectivity
void removeComponents()
{
	int i, x1, y1, x2, y2, rectangles;

	filter_size = 10;
	
	harrisFeatures();
	
	detectCircles();
	
	//detectTriangles();
	
	loadData();
	
	rectangles = DBSCAN();
	
	determineRectangles(rectangles);
	
	for(i = 0; i < rectangles; i++)
	{
		x1 = ends[2*i][1] - 5;
		
		y1 = ends[2*i][0] - 5;
		
		x2 = ends[2*i+1][1] + 5;
		
		y2 = ends[2*i+1][0] + 5;
		
		rectangle(harris_features, Point(x1, y1), Point(x2, y2), Scalar(255,255,255), CV_FILLED);
	}
	
	freeData();
	
	harris_features.copyTo(orig_img);
}
//Function to establish connectivity rules. TODO: function fails in case of jumps. Figure out a way to deal with that.
void establishConnectivity()
{
	int i, junctions;
	
	filter_size = 3;
	
	harrisFeatures();
	
	loadData();
	
	min_points = 1;
	
	epsilon = 400;
	
	junctions = DBSCAN();
	
	determineJunctions(junctions);
	
	cvtColor(harris_features, gray_image, CV_RGB2GRAY);
	int connectivity[junctions*junctions], j, connected;
	
	for(i = 0; i < junctions; i++)
	{
		for(j = 0; j < junctions; j++)
		{
			connectivity[i*junctions + j] = 0;
		}
	}
	
	for(i = 0; i < junctions - 1; i++)
	{
		for(j = i + 1; j < junctions; j++)
		{
			connected = checkConnectivity(i, j);
			
			connectivity[i*junctions + j] = connected;
			connectivity[j*junctions + i] = connected;
			
			if(connected)
			{
				line(harris_features, Point(junction_points[i][1], junction_points[i][0]),Point(junction_points[j][1], junction_points[j][0]), Scalar(0, 0, 255), 3, 8);
				//printf("%d %d %d %d\n", junction_points[i][0], junction_points[i][1], junction_points[j][0], junction_points[j][1]);
			}
		}
	}
	
	for(i = 0; i < junctions; i++)
	{
		done.push_back(0);
	}
	
	int temp;
	
	for(i = 0; i < junctions; i++)
	{
		temp = 0;
	
		for(j = 0; j < junctions; j++)
		{
			if(connectivity[i*junctions + j])
			{
				temp = 1;
				break;
			}
		}
		
		if(!temp)
		{
			done[i] = 1;
		}
	}
	
	for(i = 0; i < junctions; i++)
	{
		if(done[i] == 0)
		{
			potentials++;
			
			same_potentials.resize(potentials);
			
			same_potentials[potentials - 1].push_back(junction_points[i]);
		
			determinePotentials(junctions, connectivity, i);
			
			done[i] = 1;
		}
	}
	
	char str[5];
	
	for(i = 0; i < potentials; i++)
	{
		sprintf(str, "%d", i+1);
	
		for(j = 0; j < same_potentials[i].size(); j++)
		{
			printf("(%d,%d) ", same_potentials[i][j][0], same_potentials[i][j][1]);
			
			putText(harris_features, str, Point(same_potentials[i][j][1], same_potentials[i][j][0]), FONT_HERSHEY_PLAIN, 2, Scalar(0,0,0));
		}
		
		printf("\n");
	}
}
/****main function for testing module******/
int main(int argc, char *argv[])
{
	orig_img = imread(argv[1]);
	
	removeComponents(); 
	
	establishConnectivity();
	
	imwrite("highdensity.jpg", harris_features);
	
	return 0;
}
