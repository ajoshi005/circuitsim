

#include "highgui.h"
#include "cv.h"
#include "stdio.h"

#define h 280
#define w 320

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define MIN_AREA 1.00    
#define MAX_TOL  20.00

int main(int argc, char** argv)
	{
		if( argc == 2 )
			{
				//Image variables
				IplImage* img=cvLoadImage(argv[1]);
				IplImage* thresh_img=cvCreateImage(cvSize(w,h),8,1);
				IplImage* rimg=cvCreateImage(cvSize(w,h),8,3);
				IplImage* rimg_gray=cvCreateImage(cvSize(w,h),8,1);
				IplImage* hsvimg=cvCreateImage(cvSize(w,h),8,3);
				IplImage* src=cvCreateImage(cvSize(w,h),8,1);
				
				//Windows
				cvNamedWindow("Original Image",CV_WINDOW_AUTOSIZE);
				cvNamedWindow("Adapted Image",CV_WINDOW_AUTOSIZE);				
				cvNamedWindow("Thresholded Image",CV_WINDOW_AUTOSIZE);
				cvNamedWindow("cnt",CV_WINDOW_AUTOSIZE);
				 
				
				int h1=0;int s1=0;int v1=0;
				int h2=0;int s2=0;int v2=0;
				
				cvCreateTrackbar("H1","cnt",&h1,255,0);
				cvCreateTrackbar("H2","cnt",&h2,255,0);
				cvCreateTrackbar("S1","cnt",&s1,255,0);
				cvCreateTrackbar("S2","cnt",&s2,255,0);
				cvCreateTrackbar("V1","cnt",&v1,255,0);
				cvCreateTrackbar("V2","cnt",&v2,255,0);
				 				 
				//Resizing the image
				cvResize(img,rimg,CV_INTER_LINEAR);
				cvCvtColor(rimg, rimg_gray, CV_BGR2GRAY); 
				//Adaptive Threshold
				cvAdaptiveThreshold(rimg_gray, thresh_img, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 3, 5);
				//Changing into HSV plane
				//cvCvtColor(thresh_img,hsvimg,CV_BGR2HSV);
				
				
				while(1)

				{
				cvInRangeS(hsvimg,cvScalar(h1,s1,v1),cvScalar(h2,s2,v2),src);
				
				cvShowImage("Original Image",rimg);
				cvShowImage("Adapted Image",thresh_img);
				//cvShowImage("Thresholded Image",src);

				char c=cvWaitKey(33);
				if(c==27)
				break;
				}
		
				/*
				IplImage* dst  = cvCreateImage( cvGetSize(src), 8, 3 );
				CvMemStorage* storage = cvCreateMemStorage(0);
				CvSeq* contour = 0;    
				cvThreshold( src, src, 1, 255, CV_THRESH_BINARY );
				
				cvDilate(src, src, NULL, 2);    
				cvFindContours( src, storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
				cvZero( dst );

				for( ; contour != 0; contour = contour->h_next )
					{
						double actual_area = fabs(cvContourArea(contour, CV_WHOLE_SEQ, 0));
						if (actual_area < MIN_AREA)
							continue;

						//
						// FIXME:
						// Assuming the axes of the ellipse are vertical/perpendicular.
						//
						CvRect rect = ((CvContour *)contour)->rect;
						int A = rect.width / 2; 
						int B = rect.height / 2;
						double estimated_area = M_PI * A * B;
						double error = fabs(actual_area - estimated_area);    
						if (error > MAX_TOL)
							continue;    
						printf
						(
							 "center x: %d y: %d A: %d B: %d\n",
							 rect.x + A,
							 rect.y + B,
							 A,
							 B
						);

						CvScalar color = CV_RGB( rand() % 255, rand() % 255, rand() % 255 );
						cvDrawContours( dst, contour, color, color, -1, CV_FILLED, 8, cvPoint(0,0));
					}
				cvSaveImage("FinalImage.png", dst, 0);
				*/
										
				cvWaitKey(0);
				
				cvReleaseImage(&img);
				cvReleaseImage(&thresh_img);
				cvReleaseImage(&src);
				cvReleaseImage(&rimg);
				cvReleaseImage(&hsvimg);
				cvDestroyAllWindows();
			}
	}
	
	
	
