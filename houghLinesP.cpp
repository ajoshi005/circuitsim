//Hough Lines trial to enforce connectivity. 
//Output: Not good enough, picks up angles, therefore difficult to judge the connectivity. Try line growing.
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

int main(int argc, char *argv[])
{
	Mat orig_img, canny, img_gray;
	
	orig_img = imread(argv[1]);
	
	vector<Vec4i> lines;
	
	cvtColor(orig_img, img_gray, CV_RGB2GRAY);
	
	GaussianBlur(img_gray, img_gray, Size(5,5), 0, 0, BORDER_DEFAULT);
	
	Canny(img_gray, canny, 50, 200, 3);
	
	HoughLinesP(canny, lines, 1, CV_PI/2, 2, 20, 1);
	for(size_t i = 0; i < lines.size(); i++)
	{
		line(orig_img, Point(lines[i][0], lines[i][1]),
		Point(lines[i][2], lines[i][3]), Scalar(0, 0, 255), 3, 8);
	}
	
	imshow("Hough", orig_img);
	
	waitKey(0);
}
