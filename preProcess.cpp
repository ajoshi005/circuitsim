//Functions to remove all characters and notations in the image after adaptive thresholding. Uses clustering to find letters
//TODO: Rewrite for easy modularity with object recognizer.


#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

int num_clusters, max_index;

vector< vector<Point> > clusters;

Mat img_gray, visited;

void separateCircuit()
{
	int i, maximum = 0, size;
	
	for(i = 0; i < num_clusters; i++)
	{
		size = clusters[i].size();
	
		if(size > maximum)
		{
			maximum = size;
			
			max_index = i;
		}
	}
}

void localize()
{
	int i, j, min_x, max_x, min_y, max_y;
	
	for(i = 0; i < num_clusters; i++)
	{
		if(i == max_index)
		{
			continue;
		}
	
		min_x = 10000;
		max_x = 0;
		min_y = 10000;
		max_y = 0;
		
		for(j = 0; j < clusters[i].size(); j++)
		{
			if(clusters[i][j].x < min_x)
			{
				min_x = clusters[i][j].x;
			}
			if(clusters[i][j].x > max_x)
			{
				max_x = clusters[i][j].x;
			}
			if(clusters[i][j].y < min_y)
			{
				min_y = clusters[i][j].y;
			}
			if(clusters[i][j].y > max_y)
			{
				max_y = clusters[i][j].y;
			}
		}
		
		rectangle(img_gray, Point(min_y, min_x), Point(max_y, max_x), Scalar(255), CV_FILLED);
	}
}

void expandCluster(int x, int y)
{
	int i, j;
	
	visited.at<uchar>(x, y) = 1;
	
	clusters[num_clusters-1].push_back(Point(x, y));
	
	for(i = x-1; i <= x+1; i++)
	{
		for(j = y-1; j <= y+1; j++)
		{
			if((i > -1 && i < img_gray.rows) && (j > -1 && j < img_gray.cols) && visited.at<uchar>(i, j) == 0 && img_gray.at<uchar>(i, j) < 200)
			{				
				expandCluster(i, j);
			}
		}
	}
}

int main(int argc, char *argv[])
{
	Mat orig_img, img_blur, img_thresh;
	
	orig_img = imread(argv[1]);
	
	cvtColor(orig_img, img_gray, CV_RGB2GRAY);
	
	GaussianBlur(img_gray, img_blur, Size(5,5), 0, 0, BORDER_DEFAULT);
	
	int window_size, temp;
	
	temp = orig_img.rows / 5;
	
	if(!(temp % 2))
	{
		window_size = temp + 1;
	}
	else
	{
		window_size = temp;
	}
	
	adaptiveThreshold(img_blur, img_thresh, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, window_size, 0);
	
	int resize_factor;
	
	float resize_ratio;
	
	resize_ratio = img_thresh.cols / 650.0;
	
	resize_factor = int(resize_ratio + 0.5);
	
	resize(img_thresh, img_thresh, Size(img_thresh.cols / resize_factor, img_thresh.rows / resize_factor));
	
	img_thresh.copyTo(img_gray);
	
	visited = img_gray - img_gray;
	
	int i, j;
	
	for(i = 0; i < img_gray.rows; i++)
	{
		for(j = 0; j < img_gray.cols; j++)
		{
			if(img_gray.at<uchar>(i, j) < 200 && visited.at<uchar>(i, j) == 0)
			{
				num_clusters++;
				
				clusters.resize(num_clusters);
				
				expandCluster(i, j);
			}
		}
	}
	
	printf("%d\n", num_clusters);
	
	separateCircuit();
	
	localize();
	
	imwrite("Digits.jpg", img_gray);
	
	waitKey(0);
}
